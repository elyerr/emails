## Aplicación para enviar email Masivos 


### Configurar .env
`cp .env.example .env`

### Llaves a  modificar
`APP_NAME="Nombre empresa"`

### No olvides configurar la base de datos y tu servidor de correo
 
### Generar Key
`php artisan key:generate`

### Instalar dependencias composer
`composer install`

### Instalar dependencias npm
`npm install && npm run build`

### Migracion
`php artisan migrate`


