import axios from 'axios' 

export const api = axios.create({

  baseURL: import.meta.env.VITE_APP_API,
  timeout: 8500, 
  withCredentials: true,
  responseEncoding: 'utf8',
  
  headers: { 
    Accept: 'application/json'
  }, 
  
})

 
