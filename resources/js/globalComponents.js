import VError from  './Pages/Assets/Error.vue'
import VLabel from './Pages/Assets/Label.vue'
import VMessage from './Pages/Assets/Message.vue'
import VTable from './Pages/Assets/Table.vue'
import VPaginate from './Pages/Assets/Pagination.vue'
import VIcon from './Pages/Assets/Icon.vue'

export const components = [
    ['VError', VError], 
    ['VLabel', VLabel],
    ['VMessage', VMessage],
    ['VTable', VTable],
    ['VPaginate', VPaginate],
    ['VIcon', VIcon]
]


