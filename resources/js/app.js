import { createApp, h } from 'vue'
import { createInertiaApp } from '@inertiajs/vue3'
import { api } from './axios'
import { components } from './globalComponents'

createInertiaApp({
    resolve: name => {
        const pages = import.meta.glob('./Pages/**/*.vue', { eager: true })
        return pages[`./Pages/${name}.vue`]
    },
    setup({ el, App, props, plugin }) {
        const app = createApp({ render: () => h(App, props) }) 
        
        components.forEach(index => {
            app.component(index[0], index[1])
        });
        
        app.config.globalProperties.$api = api
        
        app.use(plugin).mount(el)
    },


})


