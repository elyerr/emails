<?php

namespace App\Http\Controllers;

use App\Http\Requests\MessageStore;
use App\Models\User;
use Illuminate\Http\Request;
use App\Notifications\SendEmail;
use Faker\Provider\ar_EG\Company;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class UserController extends Controller
{

    public function __construct(User $user)
    {
        $this->middleware('transform.request:' . $user->transformer)->only('register');
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, User $user)
    {
        $params = $this->transformFilter($user->transformer);

        $users = $this->search($user->table, $params);
 
        return $this->showAll($users,$user->transformer, 200);
    }

    public function categories(User $user)
    {
        $users = DB::table($user->table)
            ->select(DB::raw("count(category) as total, category as categoria"))
            ->groupBy('category')
            ->get();

        return response()->json(($users), 200);
    }


    public function register(Request $request, User $user)
    {
        $this->validate($request, [
            'name' => ['required','max:90'],
            'email' => ['required', 'unique:'. $user->table .',email'],
            'category' => ['required', 'max:90']
        ]);

        DB::transaction(function() use ($request, $user) {
            $user = $user->fill($request->all());
            $user->save();
        });

        return $this->showOne($user, 201);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(MessageStore $request, User $user)
    {
        if ($request->destino == 'all') {

            $users = $user->all();
        } else {

            $users = $user->where('category', $request->destino)->get();
        }
 
        Notification::send($users, new SendEmail($request->all()));

        return response()->json(['message' => "mensajes enviados"], 200);
    }

    public function destroyClient($id, User $user)
    {
         
        $client = $user->find($id);

        $client->delete();

        return $this->showOne($client);
    }
}
