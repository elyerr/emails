<?php

namespace App\Http\Controllers;
 
use Inertia\Inertia; 

class ViewController extends Controller
{
    public function create()
    {
        return Inertia::render('Dashboard/Index');
    }

    public function clients()
    {
        return Inertia::render('Client/Index');
    }
}
