<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class CompanyTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected array $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected array $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($company)
    {
        return [
            'identificador' => $company->id,
            'nombre' => $company->name,
            'correo' => $company->email,
            'categoria' => $company->category,
        ];
    }

    public static function transformRequest($index)
    {
        $atributtes = [
            'nombre' =>'name',
            'correo' => 'email',
            'categoria' => 'category'
        ];

        return isset($atributtes[$index]) ? $atributtes[$index] : null;
    }


    public static function transformResponse($index)
    {
        $atributtes = [
            'name' => 'nombre',
            'email' => 'correo',
            'category' => 'categoria',
        ];

        return isset($atributtes[$index]) ? $atributtes[$index] : null;
    }

    public static function getOriginalAttributes($index)
    {
        $atributtes = [
            'identificador' => 'id',
            'nombre' => 'name',
            'correo' => 'email',
            'categoria' => 'category'
        ];

        return isset($atributtes[$index]) ? $atributtes[$index] : null;
    }

}
