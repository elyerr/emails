<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class SendEmail extends Notification  
{
    use Queueable;

    public $data = null;

    /**
     * Create a new notification instance.
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        $files = [];

        //instancia 
        $notification = (new MailMessage());

        //creamos nuevo array
        foreach ($this->data['archivo'] as $key => $value) {
            array_push($files, [
                'file' => $value,
                'name' => $value->getClientOriginalName(),
                'mime' =>  $value->getMimeType()
            ]);
        }

        //agregamos los archivos a la instacia
        foreach ($files as $file) {
            $file_content = file_get_contents($file['file']);
            $notification->attachData(
                $file_content, $file['name'], [
                    'mime' => $file['mime']
                ]
            );
        }

        //retornamos la notificacion
        return $notification
            ->subject(ucfirst($this->data['asunto']))
            ->greeting('Es un placer saludarlo Sr(a). ' . ucfirst($notifiable->name))
            ->line('')
            ->line($this->data['contenido'])
            ->line('');
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
